import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

/**
 * Should declare all the actions
 * that could be done on Bookings in the site
 */
@Injectable({
  providedIn: 'root'
})
export class BookingDataService {
  constructor(private api: ApiService) {}

  /**
   * Get the already taken dates.
   */
  getBookedDates(): Observable<Date[]> {
    return this.api.getBookedDates();
  }
}
