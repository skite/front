import { Component, OnInit } from '@angular/core';

import * as L from 'leaflet';

@Component({
  selector: 'app-localization-map',
  templateUrl: './localization-map.component.html',
  styleUrls: ['./localization-map.component.scss']
})
export class LocalizationMapComponent implements OnInit {

  private icon = L.icon({
    iconUrl: 'assets/img/leaflet/marker-icon.png',
    iconAnchor: [25 / 2, 41],
    shadowUrl: 'assets/img/leaflet/marker-shadow.png',
    popupAnchor: [0, -41]
  });

  private map;

  private appartmentCoords: [number, number] = [45.29826, 6.58116];
  private appartmentMarker = L.marker(this.appartmentCoords, {icon: this.icon}).bindPopup('La vanoise');

  private slopesCoords: [number, number] = [45.298, 6.5819];
  private slopesMarker = L.marker(this.slopesCoords, {icon: this.icon}).bindPopup('Accès aux pistes');

  private parkingCoords: [number, number] = [45.29835, 6.5815];
  private parkingMarker = L.marker(this.parkingCoords, {icon: this.icon}).bindPopup('Accès parking P1');

  ngOnInit() {
    this.map = L.map('map', { dragging: !L.Browser.mobile});

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
      tileSize: 512,
      maxZoom: 20,
      zoomOffset: -1,
      id: 'mapbox/streets-v11',
      accessToken: 'pk.eyJ1IjoiZnN0YWluZSIsImEiOiJjamJkcDR4eWgxajZ5MzJvZjF6OWRibXVsIn0.qfuvMgqgF8G80E5iJIolMw'
  }).addTo(this.map);

    L.control.scale({imperial: false}).addTo(this.map);

    this.appartmentMarker.addTo(this.map);

    this.resetView();
  }

  resetView() {
    this.map.removeLayer(this.slopesMarker);
    this.map.removeLayer(this.parkingMarker);

    this.map.setView(this.appartmentCoords, 15);
  }

  stationView() {
    this.map.removeLayer(this.slopesMarker);
    this.map.removeLayer(this.parkingMarker);

    this.map.setView(this.appartmentCoords, 16);
  }

  slopesView() {
    this.map.removeLayer(this.parkingMarker);

    this.slopesMarker.addTo(this.map).openPopup();

    this.map.setView(this.meanCoords(this.appartmentCoords, this.slopesCoords), 18);
  }

  parkingView() {
    this.map.removeLayer(this.slopesMarker);

    this.parkingMarker.addTo(this.map).openPopup();

    this.map.setView(this.meanCoords(this.appartmentCoords, this.parkingCoords), 19);
  }

  private meanCoords(c1: [number, number], c2: [number, number]): [number, number] {
    return [(c1[0] + c2[0]) / 2, (c1[1] + c2[1]) / 2];
  }
}
