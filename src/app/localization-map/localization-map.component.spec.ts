import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalizationMapComponent } from './localization-map.component';

describe('LocalizationMapComponent', () => {
  let component: LocalizationMapComponent;
  let fixture: ComponentFixture<LocalizationMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalizationMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizationMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
