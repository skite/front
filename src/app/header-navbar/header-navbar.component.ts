import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-navbar',
  templateUrl: './header-navbar.component.html',
  styleUrls: ['./header-navbar.component.scss']
})
export class HeaderNavbarComponent implements OnInit {

  public active = 'presentation';

  constructor() { }

  ngOnInit() {
  }

  navClick(event) {
    this.active = event;
    console.log(event);
  }
}
