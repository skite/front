export class Booking {
  id: number;
  start_date: Date;
  end_date: Date;
  status: number; // 0: Pending, 1: Validated
  paid: boolean; // 0: Unpaid, 1: Paid

  first_name: string;
  last_name: string;
  email: string;
  address: string;
  address2: string;
  city: string;
  zipcode: string;
  person_count: number;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
