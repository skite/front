import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { ApiService } from './api.service';
import { AppComponent } from './app.component';
import { BookingContentComponent } from './booking-content/booking-content.component';
import { HeaderCarrouselComponent } from './header-carrousel/header-carrousel.component';
import { HeaderNavbarComponent } from './header-navbar/header-navbar.component';
import { LocalizationMapComponent } from './localization-map/localization-map.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ContactComponent } from './contact/contact.component';
import { WebcamComponent } from './webcam/webcam.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DescriptionComponent } from './description/description.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    LocalizationMapComponent,
    HeaderCarrouselComponent,
    HeaderNavbarComponent,
    BookingContentComponent,
    PresentationComponent,
    ContactComponent,
    WebcamComponent,
    DescriptionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    NgbModule,
    NgxGalleryModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    FontAwesomeModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
