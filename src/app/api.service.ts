import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  // public sendMail()

  public getBookedDates(): Observable<Date[]> {
    return this.httpClient.get<Date[]>(API_URL + '/api/bookings/taken')
      .pipe(catchError(this.handleError))
      .pipe(map(response => {
        return response.dates.map(str => new Date(str));
      }));
  }

  /**
   * Send a contact mail
   * @param contactData Data to fill the mail: ['name'], ['mail'], ['phone'], ['mobile'], ['content'] as strings
   */
  public sendContactMail(contactData: Object): Observable<any> {
    return this.httpClient.post(API_URL + '/api/mails/contact', contactData);
  }

  private handleError(error: Response | any): Observable<any> {
    console.error('ApiService::handleError', error);
    return throwError(error);
  }
}
