import { Component } from '@angular/core';
import { NgxGalleryAnimation } from '@kolkov/ngx-gallery';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent {

  private static gallery_src = 'assets/img/gallery/';

  galleryOptions = [
    {
      width: '100%',
      height: '800px',
      thumbnailsColumns: 4,
      thumbnailsSwipe: true,
      imageAnimation: NgxGalleryAnimation.Slide,
      imageInfinityMove: true,
      imageSwipe: true,
      previewCloseOnEsc: true,
      previewKeyboardNavigation: true,
      previewInfinityMove: true,
      previewCloseOnClick: true,
      previewSwipe: true
    },
    // max-width 800
    {
      breakpoint: 800,
      width: '100%',
      height: '600px',
      imagePercent: 80,
      thumbnailsPercent: 20,
      thumbnailsMargin: 20,
      thumbnailMargin: 20
    },
    // max-width 400
    {
      breakpoint: 400,
      preview: false
    }
  ];

  galleryImages = [
    PresentationComponent.name_to_img('salon'),
    PresentationComponent.name_to_img('salon2'),
    PresentationComponent.name_to_img('cuisine2'),
    PresentationComponent.name_to_img('cuisine'),
    PresentationComponent.name_to_img('cuisine3'),
    PresentationComponent.name_to_img('chambre1'),
    PresentationComponent.name_to_img('ours'),
    PresentationComponent.name_to_img('chambre2b'),
    PresentationComponent.name_to_img('sdb'),
    PresentationComponent.name_to_img('WC'),
    PresentationComponent.name_to_img('placard')
  ];

  private static name_to_img(name: String, description?: String) {
    return {
      small: PresentationComponent.gallery_src + "_" + name + '_small.JPG',
      medium: PresentationComponent.gallery_src + "_" + name + '_medium.JPG',
      big: PresentationComponent.gallery_src + "_" + name + '_big.JPG',
      description: description,
    };
  }
}
