import { Component, OnInit } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { BookingDataService } from '../booking-data.service';

@Component({
  selector: 'app-booking-content',
  templateUrl: './booking-content.component.html',
  providers: [BookingDataService],
  styleUrls: ['./booking-content.component.scss']
})
export class BookingContentComponent implements OnInit {
  // TODO: Check if phone or desktop: 1 or 2 calendar month to show

  /**
   * Indicate when the data is ready (or when no data will be received)
   */
  calendarReady = false;

  /**
   * Indicate if the data fetch has succedded or failed
   */
  failedFetchBookedDates = false;

  /**
   * Already booked dates, list of taken dates
   */
  bookedDates: Date[];

  openingDate = new Date('2024-12-04');
  closeDate = new Date('2025-05-04');

  constructor(private bookingDataService: BookingDataService) {
    this.bookingDataService.getBookedDates().subscribe(
      data => {
        this.bookedDates = data;
        this.calendarReady = true;
        this.failedFetchBookedDates = false;
      },
      err => {
        this.bookedDates = [];
        this.calendarReady = true;
        this.failedFetchBookedDates = true;
      }
    );
  }

  ngOnInit() {}

  /**
   * Return a function which add nbDay to a date
   */
  addDay(nbDay: number) {
    return (d: Date) => {
      const new_d = new Date(d);
      new_d.setDate(d.getDate() + nbDay);
      return new_d;
    };
  }

  /**
   * Cast a Date to a NgbDate
   */
  toNgbDate(d: Date) {
    return new NgbDate(d.getUTCFullYear(), d.getUTCMonth() + 1, d.getUTCDate());
  }

  /**
   * Check if the date is already booked
   */
  public isDateTaken(date: NgbDate, current: { year: number; month: number }) {
    if (this.failedFetchBookedDates) {
      return false;
    } else {
      return this.bookedDates.map(this.toNgbDate).some(e => e.equals(date));
    }
  }

  /**
   * Check if date is a start date
   * A start date is when the previous day isn't booked
   */
  public isDateTakenStart(
    date: NgbDate,
    current: { year: number; month: number }
  ) {
    if (this.failedFetchBookedDates) {
      return false;
    } else {
      return (
        this.isDateTaken(date, current) &&
        !this.bookedDates
          .map(this.addDay(+1))
          .map(this.toNgbDate)
          .some(e => e.equals(date))
      );
    }
  }

  /**
   * Check if date is a stop date
   * A stop date is when the next day isn't booked
   */
  public isDateTakenStop(
    date: NgbDate,
    current: { year: number; month: number }
  ) {
    if (this.failedFetchBookedDates) {
      return false;
    } else {
      return (
        this.isDateTaken(date, current) &&
        !this.bookedDates
          .map(this.addDay(-1))
          .map(this.toNgbDate)
          .some(e => e.equals(date))
      );
    }
  }

  public isOpenDate(date: NgbDate, current: { year: number; month: number }) {
    return this.toNgbDate(this.openingDate).equals(date);
  }

  public isCloseDate(date: NgbDate, current: { year: number; month: number }) {
    return this.toNgbDate(this.closeDate).equals(date);
  }

  public isOpeningDateValid() {
    return this.openingDate instanceof Date && !isNaN(this.openingDate.getTime());
  }

  public isCloseDateValid() {
    return this.closeDate instanceof Date && !isNaN(this.closeDate.getTime());
  }
}
