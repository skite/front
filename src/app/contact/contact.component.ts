import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalDirective } from 'angular-bootstrap-md';
import { BookingDataService } from '../booking-data.service';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  // TODO: Add mobile field to model

  @ViewChild('modalContactForm')
  public contactModalForm: ModalDirective;

  contactForm: FormGroup;

  /**
   * Indicate when the contact request status
   */
  public request = {
    sent: false,
    success: false
  };

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private bookingDataService: BookingDataService,
    private contactService: ContactService
  ) {
    this.contactForm = fb.group({
      contactFormName: ['', Validators.required],
      contactFormEmail: ['', [Validators.email, Validators.required]],
      contactFormPhone: [''],
      contactFormMobile: [''],
      contactFormText: ['', Validators.required]
    });
  }

  ngOnInit() {}

  /**
   * Submit the contact form
   */
  onSubmit() {
    if (this.contactForm.valid) {
      const contact_data = {
        name: this.contactForm.value.contactFormName,
        mail: this.contactForm.value.contactFormEmail,
        phone: this.contactForm.value.contactFormPhone,
        mobile: this.contactForm.value.contactFormMobile,
        content: this.contactForm.value.contactFormText
      };
      this.contactService.sendContactMail(contact_data).subscribe(
        res => {
          this.contactModalForm.hide();
          this.request.sent = false;
          this.request.success = true;
        },
        err => {
          console.log(err);
          this.request.sent = false;
          this.request.success = false;
        }
      );
      this.request.sent = true;
      // TODO: Add success / failure message
    } else {
      this.contactForm.get('contactFormName').markAsDirty();
      this.contactForm.get('contactFormEmail').markAsDirty();
      this.contactForm.get('contactFormPhone').markAsDirty();
      this.contactForm.get('contactFormMobile').markAsDirty();
      this.contactForm.get('contactFormText').markAsDirty();
    }
  }
}
