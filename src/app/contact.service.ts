import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  constructor(private api: ApiService) {}

  /**
   * Send a contact mail
   * @param contactData Data to fill the mail: ['name'], ['mail'], ['phone'], ['mobile'], ['content'] as strings
   */
  public sendContactMail(contactData: Object): Observable<any> {
    return this.api.sendContactMail(contactData);
  }
}
