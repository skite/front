#!/bin/bash

gallery_dir="src/assets/img/gallery"
files=$(ls ${gallery_dir} | grep ".JPG" | grep -v "_")

echo "Generating the gallery images..."

rm -f ${gallery_dir}/_*.JPG

for f in ${files}
do
	input_file="${gallery_dir}/${f}"
	output_base_file="${gallery_dir}/_$(basename ${f} .JPG)"
	convert "${input_file}" -resize 2000 "${output_base_file}_big.JPG"
	convert "${input_file}" -resize 1200 "${output_base_file}_medium.JPG"
	convert "${input_file}" -resize  500 "${output_base_file}_small.JPG"
done
